import copy
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "test_bucket_name",
    "role": "arn:aws:iam::460671877902:role/admin",
    "rules": [
        {
            "ID": "rule-1",
            "Priority": 0,
            "Filter": {
                "And": {
                    "Prefix": "logx",
                    "Tags": [{"Key": "name", "Value": "bucket_rep"}],
                }
            },
            "Status": "Enabled",
            "Destination": {
                "Bucket": "arn:aws:s3:::replication-bucket-demo-1",
                "StorageClass": "STANDARD",
            },
            "DeleteMarkerReplication": {"Status": "Disabled"},
        }
    ],
}
REPLICATION_BUCKET = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(
    hub, ctx, aws_s3_bucket_version, aws_s3_bucket_version_2, aws_iam_role, __test
):
    global PARAMETER
    PARAMETER["rules"][0]["Destination"]["Bucket"] = (
        "arn:aws:s3:::" + aws_s3_bucket_version_2["bucket"]
    )
    PARAMETER["role"] = aws_iam_role["arn"]
    PARAMETER["name"] = aws_s3_bucket_version["bucket"]
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_replication.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]

    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.s3.bucket_replication",
                name=PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.s3.bucket_replication",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_bucket_replication(hub, ctx, resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.s3.bucket_replication.describe(ctx)
    # Verify that describe output format is correct
    assert "aws.s3.bucket_replication.present" in describe_ret.get(
        PARAMETER["name"] + "-replication"
    )
    describe_params = describe_ret.get(PARAMETER["name"] + "-replication").get(
        "aws.s3.bucket_replication.present"
    )
    described_resource_map = dict(ChainMap(*describe_params))
    assert_bucket_replication(hub, ctx, described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="modify_bucket_replication_attributes", depends=["present"]
)
async def test_modify_attributes(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["rules"][0]["Filter"]["And"]["Prefix"] = "test-log"
    new_parameter["rules"][0]["Filter"]["And"]["Tags"][0]["Value"] = "test-log"

    ret = await hub.states.aws.s3.bucket_replication.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.s3.bucket_replication",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.s3.bucket_replication",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    assert_bucket_replication(hub, ctx, ret["old_state"], PARAMETER)
    assert_bucket_replication(hub, ctx, ret["new_state"], new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_exec_get", depends=["modify_bucket_replication_attributes"]
)
async def test_exec_get(hub, ctx, __test):
    global PARAMETER
    ret = await hub.exec.aws.s3.bucket_replication.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert_bucket_replication(hub, ctx, ret["ret"], PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["test_exec_get"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_replication.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert_bucket_replication(hub, ctx, ret["old_state"], PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.s3.bucket_replication",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.s3.bucket_replication",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.s3.bucket_replication.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.s3.bucket_replication",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


def assert_bucket_replication(hub, ctx, resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("role") == resource.get("role")
    assert parameters.get("rules") == resource.get("rules")
    assert parameters.get("name") == resource.get("resource_id")
