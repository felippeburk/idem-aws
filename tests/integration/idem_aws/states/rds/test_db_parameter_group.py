import time

import pytest

PARAMETER = {
    "name": "idem-test-" + str(int(time.time())),
    "db_parameter_group_family": "aurora-mysql5.7",
    "description": "description",
    "tags": {"Name": "idem-test-vpc"},
    "parameters": [
        {
            "ParameterName": "aurora_disable_hash_join",
            "ParameterValue": "1",
            "ApplyMethod": "immediate",
        }
    ],
}


@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, __test):
    """
    Create a db_parameter group.
    """
    global PARAMETER

    ret = await hub.states.aws.rds.db_parameter_group.present(ctx, **PARAMETER)

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if ctx.test:
        return
    else:
        PARAMETER["resource_id"] = ret["new_state"]["resource_id"]

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.rds.db_parameter_group.get(
        ctx, name=ret["new_state"]["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(hub, ctx):
    """
    Verify that "get" is successful after db_paramter_group has been created
    """
    get = await hub.exec.aws.rds.db_parameter_group.get(
        ctx, name=PARAMETER["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment

    # Verify that the resource_id matches for both
    assert PARAMETER["resource_id"] == get.ret["resource_id"]


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx):
    """
    Verify that "list" is successful after db_parameter_group has been created
    """
    ret = await hub.exec.aws.rds.db_parameter_group.list(
        ctx,
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_describe(hub, ctx, __test):
    """
    Describe all db_parameter group and run the "present" state the described db_parameter_group created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    get = await hub.exec.aws.rds.db_parameter_group.get(
        ctx, name=PARAMETER["resource_id"]
    )

    # Describe all instances
    ret = await hub.states.aws.rds.db_parameter_group.describe(ctx)

    assert get.ret.resource_id in ret
    assert "aws.rds.db_parameter_group.present" in ret.get(get.ret.resource_id)


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_absent(hub, ctx, __test):
    """
    Describe all db_parameter group and run the "present" state the described db_parameter_group created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    get = await hub.exec.aws.rds.db_parameter_group.get(
        ctx, name=PARAMETER["resource_id"]
    )

    # Describe all instances
    ret = await hub.states.aws.rds.db_parameter_group.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )

    assert ret["result"], ret["comment"]
    assert not ret["new_state"]
    if not get.ret:
        assert (
            hub.tool.aws.comment_utils.already_absent_comment(
                resource_type="aws.rds.db_parameter_group", name=PARAMETER["name"]
            )
            == ret["comment"]
        )
    elif ctx.test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.rds.db_parameter_group", name=PARAMETER["name"]
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.rds.db_parameter_group", name=PARAMETER["name"]
            )
            == ret["comment"]
        )
