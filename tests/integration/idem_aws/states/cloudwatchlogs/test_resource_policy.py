import copy
import time

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_resource_policy(hub, ctx):
    # Create log group
    resource_policy_temp_name = "idem-test-resource-policy-" + str(int(time.time()))
    policy_document = '{"Version":"2012-10-17","Statement":[{"Sid":"Route53LogsToCloudWatchLogs","Effect":"Allow","Principal":{"Service":"route53.amazonaws.com"},"Action":"logs:PutLogEvents","Resource":"logArn","Condition":{"StringEquals":{"aws:SourceAccount":"myAwsAccountId"},"ArnLike":{"aws:SourceArn":"myRoute53ResourceArn"}}}]}'
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Idem state --test. Create resource_policy.
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.present(
        test_ctx, name=resource_policy_temp_name, policy_document=policy_document
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.cloudwatchlogs.resource_policy '{resource_policy_temp_name}'"
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource_policy_temp_name == resource.get("name")
    assert policy_document == resource.get("policy_document")

    # create resource_policy.
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.present(
        ctx, name=resource_policy_temp_name, policy_document=policy_document
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created aws.cloudwatchlogs.resource_policy '{resource_policy_temp_name}'"
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource_policy_temp_name == resource.get("name")
    assert policy_document == resource.get("policy_document")
    assert resource_policy_temp_name == resource.get("resource_id")
    resource_id = resource.get("resource_id")

    # Describe resource_policies.
    describe_return = await hub.states.aws.cloudwatchlogs.resource_policy.describe(ctx)
    assert resource_id in describe_return
    assert describe_return.get(resource_id) and describe_return.get(resource_id).get(
        "aws.cloudwatchlogs.resource_policy.present"
    )

    # Idem state --test. update resource_policy.
    policy_document = '{"Version":"2012-10-17","Statement":[{"Sid":"Route53LogsToCloudWatchLogs","Effect":"Allow","Principal":{"Service":["ec2.amazonaws.com"]},"Action":"logs:PutLogEvents","Resource":"logArn","Condition":{"ArnLike":{"aws:SourceArn":"myRoute53ResourceArn"},"StringEquals":{"aws:SourceAccount":"myAwsAccountId"}}}]}'
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.present(
        test_ctx,
        name=resource_policy_temp_name,
        policy_document=policy_document,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert f"'{resource_policy_temp_name}' already exists" in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource_policy_temp_name == resource.get("name")
    assert policy_document == resource.get("policy_document")
    assert resource_policy_temp_name == resource.get("resource_id")

    # update resource_policy.
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.present(
        ctx,
        name=resource_policy_temp_name,
        policy_document=policy_document,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert f"'{resource_policy_temp_name}' already exists" in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource_policy_temp_name == resource.get("name")
    assert resource_policy_temp_name == resource.get("resource_id")

    # Run update resource_policy with same policy_document content. It should be a no-op.
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.present(
        ctx,
        name=resource_policy_temp_name,
        policy_document=policy_document,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert f"'{resource_policy_temp_name}' already exists" in ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource_policy_temp_name == resource.get("name")
    assert resource_policy_temp_name == resource.get("resource_id")

    # Idem state --test. Delete CloudWatchEvents Rule
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.absent(
        test_ctx,
        name=resource_policy_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would delete aws.cloudwatchlogs.resource_policy '{resource_policy_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and (not ret.get("new_state"))
    resource = ret.get("old_state")
    assert resource_policy_temp_name == resource.get("name")
    assert resource_policy_temp_name == resource.get("resource_id")

    # Delete CloudWatchEvents Rule
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.absent(
        ctx,
        name=resource_policy_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Deleted aws.cloudwatchlogs.resource_policy '{resource_policy_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and (not ret.get("new_state"))
    resource = ret.get("old_state")
    assert resource_policy_temp_name == resource.get("name")
    assert resource_policy_temp_name == resource.get("resource_id")

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.cloudwatchlogs.resource_policy.absent(
        ctx,
        name=resource_policy_temp_name,
        resource_id=resource_id,
    )
    assert (
        f"aws.cloudwatchlogs.resource_policy '{resource_policy_temp_name}' already absent"
        in ret["comment"]
    )
