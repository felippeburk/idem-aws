import copy
import time
import uuid
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-route-" + str(int(time.time())),
    "destination_cidr_block": "172.31.0.0/16",
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub, ctx, __test, aws_ec2_route_table, aws_ec2_internet_gateway, cleanup
):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["route_table_id"] = aws_ec2_route_table.get("resource_id")
    PARAMETER["gateway_id"] = aws_ec2_internet_gateway.get("resource_id")

    ret = await hub.states.aws.ec2.route.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.ec2.route", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.ec2.route", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_ec2_route(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.route.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.ec2.route.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.ec2.route.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_ec2_route(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="exec-get-by-resource_id", depends=["present"])
async def test_exec_get_by_resource_id(hub, ctx):
    # This test  is here to avoid creating another EC2 Roure Load Balancer fixture for exec.get() testing.
    ret = await hub.exec.aws.ec2.route.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert_ec2_route(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="exec-get-by-name", depends=["present"])
async def test_exec_get_by_route_table_destination(hub, ctx):
    ret = await hub.exec.aws.ec2.route.get(
        ctx,
        name=PARAMETER["name"],
        route_table_id=PARAMETER["route_table_id"],
        destination_cidr_block=PARAMETER["destination_cidr_block"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert_ec2_route(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["exec-get-by-name"])
async def test_update(hub, ctx, aws_ec2_nat_gateway, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["gateway_id"] = None
    new_parameter["nat_gateway_id"] = aws_ec2_nat_gateway.get("resource_id")
    ret = await hub.states.aws.ec2.route.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.ec2.route", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
    old_resource = ret["old_state"]
    assert_ec2_route(old_resource, PARAMETER)
    resource = ret["new_state"]
    assert_ec2_route(resource, new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.route.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert_ec2_route(old_resource, PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.ec2.route", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.ec2.route", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.route.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.route", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_route_absent_with_none_resource_id(hub, ctx):
    route_temp_name = "idem-test-route-" + str(uuid.uuid4())
    # Delete route with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.route.absent(
        ctx, name=route_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.route", name=route_temp_name
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.route.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_ec2_route(resource, parameters):
    assert parameters.get("route_table_id") == resource.get("route_table_id")
    assert parameters.get("destination_cidr_block") == resource.get(
        "destination_cidr_block"
    )
    assert parameters.get("gateway_id") == resource.get("gateway_id")
    assert parameters.get("nat_gateway_id") == resource.get("nat_gateway_id")
