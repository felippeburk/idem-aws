import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["tags"]["new_tag"] = "new_value"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert "new_tag" not in ret["changes"]["old"]["tags"]
        assert ret["changes"]["new"]["tags"]["new_tag"] == "new_value"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
# @pytest.mark.dependency(depends=["present"])
async def test_update(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    # Use a list dict this time for tags
    state["tags"] = [
        {
            "Key": "new_tag",
            "Value": "changed_value",
        }
    ]

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["old"]["tags"]["new_tag"] == "new_value"
        assert ret["changes"]["new"]["tags"]["new_tag"] == "changed_value"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    # Remove the changed tag from previous tests so that it gets deleted
    # state["tags"]["new_tag"] = "changed_value"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], ret["comment"]

    # Before/during creation, report the changes made
    if __test <= 1:
        assert "new_tag" not in ret["changes"]["new"]["tags"]
    # After creation, no changes made with/without test
    else:
        assert not ret["changes"] is None
