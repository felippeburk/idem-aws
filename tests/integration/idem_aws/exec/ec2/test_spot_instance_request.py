import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_spot_instance_request):
    spot_instance_request_get_name = "idem-test-exec-get-spot_instance_request-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.ec2.spot_instance_request.get(
        ctx,
        name=spot_instance_request_get_name,
        resource_id=aws_ec2_spot_instance_request["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_ec2_spot_instance_request["resource_id"] == resource.get(
        "spot-instance-request-id"
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx, aws_ec2_spot_instance_request):
    ret = await hub.exec.aws.ec2.spot_instance_request.get(
        ctx,
        name="fake-id",
        resource_id="fake-id",
    )
    assert "comment" in ret
    assert ret["ret"] is None
    assert "Get aws.ec2.spot_instance_request 'fake-id' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_spot_instance_request):
    spot_instance_request_list_name = (
        "idem-test-exec-list-spot_instance_request-" + str(int(time.time()))
    )
    filters = [
        {
            "name": "spot-instance-request-id",
            "values": [aws_ec2_spot_instance_request["resource_id"]],
        }
    ]
    ret = await hub.exec.aws.ec2.spot_instance_request.list(
        ctx,
        name=spot_instance_request_list_name,
        filters=filters,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource = {}
    for entry in ret["ret"]:
        if (
            entry["spot-instance-request-id"]
            == aws_ec2_spot_instance_request["resource_id"]
        ):
            resource = entry
    assert aws_ec2_spot_instance_request["resource_id"] == resource.get(
        "spot-instance-request-id"
    )
