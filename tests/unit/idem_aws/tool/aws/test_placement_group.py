# pylint: disable=W9903


def test_convert_raw_attributes_to_present(hub, ctx):
    present_attributes = {
        "resource_id": "test_placement_group_name",
        "group_id": "placement_group-123456789",
        "group_name": "test_placement_group_name",
        "state": "available",
        "strategy": "cluster",
        "partition_count": 123,
        "spread_level": "rack",
        "tags": {"Env": "test"},
    }

    raw_attributes = {
        "GroupId": "placement_group-123456789",
        "GroupName": "test_placement_group_name",
        "GroupArn": "test_placement_group_arn",
        "State": "available",
        "Strategy": "cluster",
        "PartitionCount": 123,
        "SpreadLevel": "rack",
        "Tags": [{"Key": "Env", "Value": "test"}],
    }

    result = hub.tool.aws.ec2.conversion_utils.convert_raw_placement_group_to_present(
        raw_attributes
    )
    assert result == present_attributes
