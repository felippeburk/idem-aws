# This SLS file will find and remove VPCs that were created during tests and their dependencies
# There is no configuration needed, simply run `idem state example/cleanup/vpc.sls`

# Find leftover VPCs
orphan-vpcs:
  exec.run:
    - path: aws.ec2.vpc.list
    - kwargs:
        name: null
        filters:
          - name: "tag:Name"
            values:
              - "idem-fixture-vpc-*"

#!require:orphan-vpcs
{% for vpc in hub.idem.arg_bind.resolve('${exec:orphan-vpcs}') %}
orphan-vpc-dependencies-{{ vpc.resource_id }}:
  render.block:
    - sls: >
        {% for dependency in ("subnet", "route_table", "instance") %}
        orphan-{{ dependency }}-{{ vpc.resource_id }}:
          exec.run:
            - path: aws.ec2.{{ dependency }}.list
            - kwargs:
                name: null
                filters:
                  - name: "vpc-id"
                    values:
                      - {{ vpc.resource_id }}

        #!require:orphan-{{ dependency }}-{{ vpc.resource_id }}
        {% raw %}
        {% for resource in hub.idem.arg_bind.resolve('${exec:orphan-{{ dependency }}-{{ vpc.resource_id }}}') %}

        cleanup-{{ resource.resource_id }}:
          aws.ec2.{{ dependency }}.absent:
            - resource_id: {{ resource.resource_id }}

        {% endfor %}
        {% endraw %}

        {% endfor %}

        cleanup-{{ vpc['resource_id'] }}:
          aws.ec2.vpc.absent:
            - resource_id: {{ vpc['resource_id'] }}
{% endfor %}
